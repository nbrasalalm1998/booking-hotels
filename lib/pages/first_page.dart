import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hotel_bookings/pages/home_page.dart';

class FirstPage extends StatefulWidget {
  FirstPage({super.key});

  @override
  State<FirstPage> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  late int index;
  List pages = [
    HomePage(),
    Container(),
    Container(),
    Container(),
  ];
  @override
  void initState() {
    // TODO: implement initState
    index = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pages[index],
      bottomNavigationBar: BottomNavigationBar(
          showUnselectedLabels: false,
          showSelectedLabels: false,
          iconSize: 25,
          elevation: 3,
          currentIndex: index,
          onTap: (value) {
            setState(() {
              index = value;
            });
          },
          items: [
            BottomNavigationBarItem(
                icon: SvgPicture.asset('assets/svgs/homepage.svg'),
                label: 'home'),
            BottomNavigationBarItem(
                icon: SvgPicture.asset('assets/svgs/clock.svg'), label: 'time'),
            BottomNavigationBarItem(
                icon: SvgPicture.asset('assets/svgs/maps.svg'),
                label: 'alamat'),
            BottomNavigationBarItem(
                icon: SvgPicture.asset('assets/svgs/person.svg'),
                label: 'person')
          ]),
    );
  }
}
