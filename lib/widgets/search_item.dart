import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_svg/flutter_svg.dart';

class searchItem extends StatelessWidget {
  const searchItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 372,
      height: 58,
      child: TextField(
        autocorrect: false,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          hintText: 'Search hotel',
          hintStyle: GoogleFonts.urbanist(
              fontWeight: FontWeight.w500, fontSize: 16, color: Colors.black54),
          fillColor: Color(0xffECECEC),
          filled: true,
          prefixIcon: Image.asset('assets/images/search-icon.png'),
          border: OutlineInputBorder(
              borderSide: BorderSide.none,
              borderRadius: BorderRadius.circular(50)),
          suffixIcon: Padding(
            padding:
                const EdgeInsets.only(top: 11, left: 15, bottom: 11, right: 15),
            child: Container(
              height: 36,
              width: 36,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(18),
                  color: Colors.white,
                  image: DecorationImage(
                      image: AssetImage('assets/images/ellipse-icon.png'))),
            ),
          ),
        ),
      ),
    );
  }
}
