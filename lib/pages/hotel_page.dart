import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_svg/svg.dart';
import 'package:readmore/readmore.dart';

class HotelPage extends StatefulWidget {
  const HotelPage({super.key});

  @override
  State<HotelPage> createState() => _HotelPageState();
}

class _HotelPageState extends State<HotelPage> {
  String text =
      'Hotel Room means an area that is designed and constructed to be occupied by one or more persons on Hotel Property, which is separate Hotel Room means an area that is designed and constructed to be occupied by one or more persons on Hotel Property, which is separate';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(219, 255, 255, 255),
      body: SafeArea(
        child: ListView(
          children: [
            Column(
              children: [
                Stack(
                  children: [
                    //sebuah container untuk menampung
                    Container(
                      height: 437,
                      width: double.infinity,
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  'assets/images/background-hotel.png'),
                              fit: BoxFit.cover)),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                        //membuat row untuk icon kembali dan juga setelan
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 15),
                          child: Row(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Container(
                                    height: 30,
                                    width: 30,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      color: Colors.white24,
                                    ),
                                    child: SvgPicture.asset(
                                      'assets/svgs/arrow-back.svg',
                                    )),
                              ),
                              Spacer(),
                              Container(
                                  height: 30,
                                  width: 30,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    color: Colors.white24,
                                  ),
                                  child: SvgPicture.asset(
                                    'assets/svgs/menu.svg',
                                    fit: BoxFit.cover,
                                  )),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 3.6,
                        ),
                        //membuat container untuk menampung deskripsi hotel
                        Container(
                          width: double.infinity,
                          height: 432,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Colors.white),
                          child:
                              //membuat fitur deskripsi hotel beserta rating dan contoh gambar interiornya
                              Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 17.0, vertical: 25),
                            child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Swiss Hotel',
                                            style: GoogleFonts.urbanist(
                                                fontWeight: FontWeight.w800,
                                                fontSize: 26),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            '211B Baker Street, London, England',
                                            style: GoogleFonts.urbanist(
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Row(
                                            children: [
                                              SizedBox(
                                                height: 19,
                                                width: 19,
                                                child: SvgPicture.asset(
                                                    'assets/svgs/stars.svg'),
                                              ),
                                              SizedBox(
                                                height: 19,
                                                width: 19,
                                                child: SvgPicture.asset(
                                                    'assets/svgs/stars.svg'),
                                              ),
                                              SizedBox(
                                                height: 19,
                                                width: 19,
                                                child: SvgPicture.asset(
                                                    'assets/svgs/stars.svg'),
                                              ),
                                              SizedBox(
                                                height: 19,
                                                width: 19,
                                                child: SvgPicture.asset(
                                                    'assets/svgs/stars.svg'),
                                              ),
                                              SizedBox(
                                                height: 19,
                                                width: 19,
                                                child: SvgPicture.asset(
                                                    'assets/svgs/stars.svg'),
                                              ),
                                              SizedBox(
                                                width: 3,
                                              ),
                                              Text(
                                                '4.9',
                                                style: GoogleFonts.urbanist(
                                                    fontWeight: FontWeight.w700,
                                                    fontSize: 14),
                                              ),
                                              Text(
                                                '-1231 Reviews',
                                                style: GoogleFonts.urbanist(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 14,
                                                    color: Colors.black54),
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                      Spacer(),
                                      //membuat icon location
                                      Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          Container(
                                            height: 57,
                                            width: 57,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(40),
                                                border: Border.all(
                                                    color: Colors.black26,
                                                    width: 4)),
                                          ),
                                          Container(
                                            height: 30,
                                            width: 30,
                                            child: SvgPicture.asset(
                                                'assets/svgs/location-pin.svg'),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 28,
                                  ),
                                  //membuat konten deskripsi
                                  Text(
                                    'Description',
                                    style: GoogleFonts.urbanist(
                                        fontWeight: FontWeight.w800,
                                        fontSize: 20),
                                  ),
                                  ReadMoreText(
                                    trimMode: TrimMode.Line,
                                    trimLines: 3,
                                    text,
                                    style: GoogleFonts.urbanist(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16,
                                        color: Colors.black54),
                                    trimCollapsedText: ' Show more',
                                    moreStyle: GoogleFonts.urbanist(
                                        fontWeight: FontWeight.w800,
                                        fontSize: 16,
                                        color: Color(0xff00A3FF)),
                                    lessStyle: GoogleFonts.urbanist(
                                        fontWeight: FontWeight.w800,
                                        fontSize: 16,
                                        color: Color(0xff00A3FF)),
                                    trimExpandedText: ' Show less',
                                  ),
                                  SizedBox(
                                    height: 48,
                                  ),
                                  SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      children: [
                                        Container(
                                          height: 85,
                                          width: 85,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/kamar-hotel.png'),
                                                  fit: BoxFit.cover)),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 10.0),
                                          child: Container(
                                            height: 85,
                                            width: 85,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                image: DecorationImage(
                                                    image: AssetImage(
                                                        'assets/images/kamar-hotel.png'),
                                                    fit: BoxFit.cover)),
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 10.0),
                                          child: Container(
                                            height: 85,
                                            width: 85,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                image: DecorationImage(
                                                    image: AssetImage(
                                                        'assets/images/kamar-hotel.png'),
                                                    fit: BoxFit.cover)),
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 10.0),
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: [
                                              Container(
                                                height: 85,
                                                width: 85,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    image: DecorationImage(
                                                        image: AssetImage(
                                                            'assets/images/kamar-hotel.png'),
                                                        fit: BoxFit.cover)),
                                              ),
                                              Container(
                                                height: 85,
                                                width: 85,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    color: Colors.black54),
                                                child: Center(
                                                    child: Text(
                                                  '5+',
                                                  style: GoogleFonts.urbanist(
                                                      fontWeight:
                                                          FontWeight.w800,
                                                      fontSize: 24,
                                                      color: Colors.white),
                                                )),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 45,
                        ),
                        //membuat button simpan dan juga book now
                        Container(
                          margin:
                              EdgeInsets.only(left: 15, right: 15, bottom: 15),
                          child: Row(
                            children: [
                              Container(
                                height: 53,
                                width: 53,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Colors.white),
                                child: SvgPicture.asset(
                                    'assets/svgs/bookmark.svg'),
                              ),
                              Spacer(),
                              Container(
                                width: 273,
                                height: 60,
                                decoration: BoxDecoration(
                                    color: Color(0xff5177FF),
                                    borderRadius: BorderRadius.circular(50)),
                                child: Material(
                                  color: Colors.transparent,
                                  borderRadius: BorderRadius.circular(50),
                                  child: InkWell(
                                    onTap: () {},
                                    borderRadius: BorderRadius.circular(50),
                                    child: Center(
                                      child: Text(
                                        'Book now',
                                        style: GoogleFonts.urbanist(
                                            fontWeight: FontWeight.w800,
                                            fontSize: 20,
                                            color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
