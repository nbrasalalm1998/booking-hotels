import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_svg/flutter_svg.dart';

class titleItem extends StatelessWidget {
  const titleItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 74,
      width: 373,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Discover your',
            style: GoogleFonts.urbanist(
                fontWeight: FontWeight.w500,
                fontSize: 24,
                color: Colors.black54),
          ),
          Text(
            'perfect place to stay',
            style: GoogleFonts.urbanist(
                fontWeight: FontWeight.w800, fontSize: 24, color: Colors.black),
          ),
        ],
      ),
    );
  }
}
