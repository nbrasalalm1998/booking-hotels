import 'package:flutter/material.dart';
import '../widgets/title_item.dart';
import '../widgets/search_item.dart';
import '../widgets/imagehotel_item.dart';
import '../widgets/rowdaftar_item.dart';
import '../widgets/appbar_item.dart';
import '../widgets/listdaftarhotel_item.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          title: appbarItem(),
          actions: [
            IconButton(
              onPressed: () {},
              icon: Image.asset(
                'assets/images/alarm-icon.png',
              ),
            )
          ],
        ),
        body: ListView(
          children: [
            Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                //membuat sebuah container untuk menampung sebuah text
                titleItem(),
                //membuat textfield untuk fitur search hotel
                searchItem(),
                SizedBox(
                  height: 40,
                ),
                //membuat fitur row kategori
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 1),
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TabBar(
                        isScrollable: true,
                        unselectedLabelStyle: GoogleFonts.urbanist(
                            fontWeight: FontWeight.w600, fontSize: 16),
                        unselectedLabelColor: Color(0xff727272),
                        indicator: BoxDecoration(
                            color: Color(0xff31355C),
                            borderRadius: BorderRadius.circular(20)),
                        tabs: [
                          Text('Home'),
                          Text('Apartements'),
                          Text('Condo'),
                          Text('Mansion'),
                          Text('Mansion'),
                        ]),
                  ),
                ),
                SizedBox(
                  height: 13,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 9,
                  child: TabBarView(children: [
                    Column(
                      children: [
                        //membuat contoh gambar hotel lengkap dengan nama,harga dan juga rating
                        imagehotelItem(),
                        SizedBox(
                          height: 35,
                        ),
                        rowdaftarItem(),
                        SizedBox(
                          height: 15,
                        ),
                        listdaftarhotelItem(),
                      ],
                    ),
                    Container(),
                    Container(),
                    Container(),
                    Container(),
                  ]),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
