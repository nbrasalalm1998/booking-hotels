import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class rowdaftarItem extends StatelessWidget {
  const rowdaftarItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        children: [
          Text(
            'Hotels Nearby',
            style: GoogleFonts.urbanist(
                fontWeight: FontWeight.w800, fontSize: 24, color: Colors.black),
          ),
          Spacer(),
          InkWell(
            onTap: () {},
            child: Text(
              'Show all',
              style: GoogleFonts.urbanist(
                  fontWeight: FontWeight.w500,
                  fontSize: 15,
                  color: Colors.black54),
            ),
          ),
        ],
      ),
    );
  }
}
