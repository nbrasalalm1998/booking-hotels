# hotel_bookings

Aplikasi hotel booking ini dibuat untuk portofolio pribadi.
Aplikasi ini dibuat menggunakan framework flutter dan bahasa pemrograman dart

## Hasil
Gambar hasil :

![project](/uploads/7a42401c73d9cd9e5f77fc5f9922d7d1/WhatsApp_Image_2023-01-24_at_00.34.17.jpeg) ![project](/uploads/6996c18893c5f2da689d475623a0ee66/WhatsApp_Image_2023-01-24_at_00.34.17__1_.jpeg)

## Tautan
Figma : [figma](https://www.figma.com/file/04g5cfX6tUuwvD6OSip3dJ/Untitled?node-id=0%3A1&t=nHXwPopUWDC5vL2P-1)

Youtube : [youtube](https://youtube.com/%40artofcoding22)

Instagram : [ig](https://www.instagram.com/drk_22drynnnnnn/)

Github : [github](https://github.com/davidriyan)

Email : davidriyan321@gmail.com