import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class appbarItem extends StatelessWidget {
  const appbarItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showModalBottomSheet(
          backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20))),
          context: context,
          builder: (context) => SizedBox(
            height: 400,
            child: ListView(
              children: [
                Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        height: 8,
                        width: 45,
                        decoration: BoxDecoration(
                            color: Colors.black54,
                            borderRadius: BorderRadius.circular(4)),
                      ),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      leading: Image.asset('assets/images/location-icon.png'),
                      title: Text(
                        'Jakarta',
                        style: GoogleFonts.urbanist(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Colors.black54),
                      ),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      leading: Image.asset('assets/images/location-icon.png'),
                      title: Text(
                        'Paris',
                        style: GoogleFonts.urbanist(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Colors.black54),
                      ),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      leading: Image.asset('assets/images/location-icon.png'),
                      title: Text(
                        'Roma',
                        style: GoogleFonts.urbanist(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Colors.black54),
                      ),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      leading: Image.asset('assets/images/location-icon.png'),
                      title: Text(
                        'Tokyo',
                        style: GoogleFonts.urbanist(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Colors.black54),
                      ),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      leading: Image.asset('assets/images/location-icon.png'),
                      title: Text(
                        'Madrid',
                        style: GoogleFonts.urbanist(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Colors.black54),
                      ),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      leading: Image.asset('assets/images/location-icon.png'),
                      title: Text(
                        'Berlin',
                        style: GoogleFonts.urbanist(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Colors.black54),
                      ),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      leading: Image.asset('assets/images/location-icon.png'),
                      title: Text(
                        'Makkah',
                        style: GoogleFonts.urbanist(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Colors.black54),
                      ),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      leading: Image.asset('assets/images/location-icon.png'),
                      title: Text(
                        'Seoul',
                        style: GoogleFonts.urbanist(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Colors.black54),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
      child: Container(
        width: 199,
        height: 45,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(100),
            border: Border.all(color: Colors.black12, width: 2)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Image.asset('assets/images/location-icon.png'),
            Text(
              '211B Baker Street',
              style: GoogleFonts.urbanist(
                  fontWeight: FontWeight.w800,
                  fontSize: 12,
                  color: Colors.black45),
            ),
            Image.asset('assets/images/arrowup-icon.png'),
          ],
        ),
      ),
    );
  }
}
