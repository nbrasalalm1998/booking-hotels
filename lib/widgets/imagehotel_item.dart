import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hotel_bookings/pages/hotel_page.dart';

class imagehotelItem extends StatelessWidget {
  const imagehotelItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        child: Row(
          children: [
            InkWell(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) {
                    return HotelPage();
                  },
                ));
              },
              child: Container(
                width: 274,
                height: 170,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    image: DecorationImage(
                        image: AssetImage('assets/images/hotel-1.png'),
                        fit: BoxFit.cover)),
                child:
                    //membuat text nama hotel, harga, dan rating
                    Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      gradient: LinearGradient(
                          colors: [
                            Color.fromARGB(143, 0, 0, 0),
                            Colors.black12,
                            Colors.black12
                          ],
                          begin: Alignment.bottomCenter,
                          end: Alignment.topCenter)),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10.0, vertical: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Swiss Hotel',
                          style: GoogleFonts.urbanist(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                              color: Colors.white),
                        ),
                        Row(
                          children: [
                            Text(
                              'Rp.300.000',
                              style: GoogleFonts.urbanist(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16,
                                  color: Colors.white),
                            ),
                            Spacer(),
                            Row(
                              children: [
                                SizedBox(
                                    height: 25,
                                    width: 25,
                                    child: SvgPicture.asset(
                                      'assets/svgs/stars.svg',
                                      fit: BoxFit.cover,
                                    )),
                                Text(
                                  '4.8',
                                  style: GoogleFonts.urbanist(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16,
                                      color: Colors.white),
                                )
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            //membuat image hotel kedua
            Container(
              margin: EdgeInsets.only(left: 3),
              width: 274,
              height: 170,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  image: DecorationImage(
                      image: AssetImage('assets/images/hotel-2.png'),
                      fit: BoxFit.cover)),
              child:
                  //membuat text nama hotel, harga, dan rating
                  Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    gradient: LinearGradient(
                        colors: [
                          Color.fromARGB(143, 0, 0, 0),
                          Colors.black12,
                          Colors.black12
                        ],
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter)),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 10.0, vertical: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'French Hotel',
                        style: GoogleFonts.urbanist(
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                            color: Colors.white),
                      ),
                      Row(
                        children: [
                          Text(
                            'Rp.300.000',
                            style: GoogleFonts.urbanist(
                                fontWeight: FontWeight.w500,
                                fontSize: 16,
                                color: Colors.white),
                          ),
                          Spacer(),
                          Row(
                            children: [
                              SizedBox(
                                  height: 25,
                                  width: 25,
                                  child: SvgPicture.asset(
                                    'assets/svgs/stars.svg',
                                    fit: BoxFit.cover,
                                  )),
                              Text(
                                '4.8',
                                style: GoogleFonts.urbanist(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16,
                                    color: Colors.white),
                              )
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
